// Déclarations des costantes
const dataLowercase = "azertyuiopqsdfghjklmwxcvbn";
const dataUppercase = dataLowercase.toLocaleUpperCase();
const dataNumbers = "0123456789";
const dataSymbols = "$^!?*µ§,:&#éçà@";
const rangeValue = document.getElementById("password-length");
const passwordOuput = document.getElementById("password-output");

function generatePassword() {
  // les variables
  let data = [];
  let password = "";
  // les conditions afin de stocker les constantes dans la data
  if (lowercase.checked) data.push(...dataLowercase);
  if (uppercase.checked) data.push(...dataUppercase);
  if (numbers.checked) data.push(...dataNumbers);
  if (symbols.checked) data.push(...dataSymbols);

  if (data.length === 0) {
    alert("Veuillez sélectionner des critères");
    return;
  }
  for (i = 0; i < rangeValue.value; i++) {
    password += data[Math.floor(Math.random() * data.length)];
  }
  passwordOuput.value = password;
  passwordOuput.select();
  document.execCommand("copy");
  generateButton.textContent = "Copié !";
  setTimeout(() => {
    generateButton.textContent = "Générer mot de passe";
  }, 2000);
}
generateButton.addEventListener("click", generatePassword);
